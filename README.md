# NLP se SpaCy knihovnou

- Pro python verze 3

## Požadavky na instalaci

### Anaconda nebo Miniconda

Dle instrukcí na https://docs.anaconda.com/anaconda/install/

Po instalaci provést update.

```bash
conda update -n base -c defaults conda
```

### Spacy knihovna

Instrukce pro instalaci na https://spacy.io/usage.

```bash
conda install -c conda-forge spacy
conda install -c conda-forge spacy-lookups-data
```

### Jazykové modely

- střední model pro angličtinu `en_core_web_md`
- volitelně větší model pro angličtinu `en_core_web_lg`

Instrukce pro instalaci na https://spacy.io/usage/models

```bash
python -m spacy download en_core_web_md
```

### Keras pro trénování klasifikátoru

Pro pokročilejší techniky v klasifikačních úlohách se hodí Keras. Viz https://anaconda.org/conda-forge/keras. Jako závislost se automaticky nainstaluje také tenserflow.

```bash
conda install -c conda-forge keras
```
